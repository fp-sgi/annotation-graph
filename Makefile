.PHONY: clean all pdf

r_sources := $(shell find . -type f -name '*.R')
r_targets := $(patsubst %.R, %.pdf, $(r_sources))

all: pdf
pdf: $(r_targets)

%.pdf : %.R
	@Rscript $<

clean:
	@rm *.pdf *.jpg
