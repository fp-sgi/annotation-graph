# Annotations

[![build status](https://gitlab.interactivesystems.info/fp_social_game_interaction/Annotations/badges/master/build.svg)](https://gitlab.interactivesystems.info/fp_social_game_interaction/Annotations/commits/master)

## Downloads der Grafik

Diese Downloads repräsentieren den aktuellen Stand des `master`-Branches:

* [PDF](http://fp-sgi.s3-website.eu-central-1.amazonaws.com/Annotations/Annotations.pdf)
* [JPEG](http://fp-sgi.s3-website.eu-central-1.amazonaws.com/Annotations/Annotations.jpg)
